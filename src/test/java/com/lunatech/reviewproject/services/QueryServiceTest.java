package com.lunatech.reviewproject.services;

import com.lunatech.reviewproject.TestAncestor;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author nvkmi
 */

public class QueryServiceTest extends TestAncestor {

    @MockBean
    private QueryService queryService;

    @Test
    public void getAirportsWithRunwaysInCountryWasCalled_ok() {
        queryService.getAirportsWithRunwaysInCountry("cz");
        verify(queryService, times(1)).getAirportsWithRunwaysInCountry("cz");
    }
}
