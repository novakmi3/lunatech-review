package com.lunatech.reviewproject.services;

import com.lunatech.reviewproject.TestAncestor;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author nvkmi
 */

public class ReportServiceTest  extends TestAncestor {
    
    @MockBean
    private ReportService reportService;

    @Test
    public void findCountriesWithHighestNumberOfAirportsWasCalled_ok() {
        reportService.findCountriesWithHighestNumberOfAirports();
        verify(reportService, times(1)).findCountriesWithHighestNumberOfAirports();
    }
    
    @Test
    public void findCountriesWithLowestNumberOfAirportsWasCalled_ok() {
        reportService.findCountriesWithLowestNumberOfAirports();
        verify(reportService, times(1)).findCountriesWithLowestNumberOfAirports();
    }
    
    @Test
    public void findTypeOfRunwayForCountryWasCalled_ok() {
        reportService.findTypeOfRunwayForCountry();
        verify(reportService, times(1)).findTypeOfRunwayForCountry();
    }
    
    @Test
    public void findMostCommonRunwaysWasCalled_ok() {
        reportService.findMostCommonRunways(2);
        verify(reportService, times(1)).findMostCommonRunways(2);
    }
}
