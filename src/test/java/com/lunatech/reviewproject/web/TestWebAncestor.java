package com.lunatech.reviewproject.web;

import com.lunatech.reviewproject.TestAncestor;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author nvkmi
 */

public abstract class TestWebAncestor extends TestAncestor {
    
    protected MockMvc mock;
    
    @Autowired
    private WebApplicationContext wac;
    
    @Before
    public void setup() {
        this.mock = MockMvcBuilders.webAppContextSetup(wac).build();
    }
}
