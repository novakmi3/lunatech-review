package com.lunatech.reviewproject.web;

import com.lunatech.reviewproject.entities.Country;
import com.lunatech.reviewproject.services.QueryService;
import org.hamcrest.Matchers;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.junit.Test;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 *
 * @author nvkmi
 */

public class QueryControllerTest extends TestWebAncestor {
    
    @MockBean
    private QueryService queryService;
    
    @Test
    public void showQuery_ok() throws Exception {
        when(queryService.getAirportsWithRunwaysInCountry(anyString())).thenReturn(new Country());
        mock.perform(get("/query"))
                .andExpect(status().isOk())
                .andExpect(view().name("query"))
                .andExpect(model().attribute("country", Matchers.hasProperty("id", is(nullValue()))))
                .andExpect(model().attribute("country", Matchers.hasProperty("code", is(nullValue()))))
                .andExpect(model().attribute("country", Matchers.hasProperty("name", is(nullValue()))));
    }
    
    @Test
    public void showQuery_withGivenValidCountry_ok() throws Exception {
        when(queryService.getAirportsWithRunwaysInCountry(anyString())).thenReturn(makeCountry());
        mock.perform(get("/query?name=cz"))
                .andExpect(status().isOk())
                .andExpect(view().name("query"))
                .andExpect(model().attribute("country", Matchers.hasProperty("id", is(1L))))
                .andExpect(model().attribute("country", Matchers.hasProperty("code", is("CZ"))))
                .andExpect(model().attribute("country", Matchers.hasProperty("name", is("Czech Republic"))));
        
    }
    
    @Test
    public void showQuery_withGivenInvalidCountry_ok() throws Exception {
        mock.perform(get("/query?name=cz*"))
                .andExpect(status().isOk())
                .andExpect(view().name("query"))
                .andExpect(model().attribute("error", is("Country name cannot contain numbers or special characters.")))
                .andExpect(model().attribute("country", Matchers.hasProperty("name", is("cz*"))));     
    }
    
    private Country makeCountry() {
        return new Country(1L, "CZ", "Czech Republic");
    }
}
