package com.lunatech.reviewproject.web;

import org.junit.Test;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 *
 * @author nvkmi
 */

public class HomeControllerTest extends TestWebAncestor {
    
    @Test
    public void showHomePage_ok() throws Exception {
       mock.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));
    }
}
