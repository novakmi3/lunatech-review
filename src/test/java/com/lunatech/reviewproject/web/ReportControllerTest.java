package com.lunatech.reviewproject.web;

import com.lunatech.reviewproject.entities.Country;
import com.lunatech.reviewproject.services.ReportService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 *
 * @author nvkmi
 */

public class ReportControllerTest extends TestWebAncestor {

    @MockBean
    private ReportService reportService;

    @Test
    public void showReport_ok() throws Exception {
        when(reportService.findCountriesWithHighestNumberOfAirports()).thenReturn(makeCountryList());
        when(reportService.findCountriesWithLowestNumberOfAirports()).thenReturn(makeCountryList());
        when(reportService.findTypeOfRunwayForCountry()).thenReturn(makeTypeOfRunwaysPerCountry());
        when(reportService.findMostCommonRunways(anyInt())).thenReturn(makeRunwayTypeList());

        mock.perform(get("/report"))
                .andExpect(status().isOk())
                .andExpect(view().name("report"))
                .andExpect(model().attribute("countriesH", IsCollectionWithSize.hasSize(2)))
                .andExpect(model().attribute("countriesL", IsCollectionWithSize.hasSize(2)))
                .andExpect(model().attribute("runways", IsMapContaining.hasKey("cz")))
                .andExpect(model().attribute("runways", IsMapContaining.hasKey("de")))
                .andExpect(model().attribute("commonRunways", IsCollectionWithSize.hasSize(2)));

    }

    private List<Country> makeCountryList() {
        List<Country> ret = new ArrayList<>();
        ret.add(new Country(1L, "CZ", "Czech Republic"));
        ret.add(new Country(2L, "FR", "France"));
        return ret;
    }

    private Map<String, List<String>> makeTypeOfRunwaysPerCountry() {
        Map<String, List<String>> ret = new TreeMap<>();
        ret.put("cz", new ArrayList<>());
        ret.put("de", new ArrayList<>());
        return ret;
    }

    private List<String> makeRunwayTypeList() {
        List<String> ret = new ArrayList<>();
        ret.add("type1");
        ret.add("type2");
        return ret;
    }
}
