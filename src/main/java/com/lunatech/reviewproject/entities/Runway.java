package com.lunatech.reviewproject.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author nvkmi
 */

@Entity
@Table(name = "runways")
public class Runway {
    
    @Id
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Airport airport;
    
    @Column(nullable = false)
    private String surface;
    
    @Column(nullable = false)
    private String identification;

    public Runway() {
    }

    public Runway(Long id, Airport airport, String surface, String identification) {
        this.id = id;
        this.airport = airport;
        this.surface = surface;
        this.identification = identification;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Airport getAirport() {
        return airport;
    }

    public void setAirport(Airport airport) {
        this.airport = airport;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    @Override
    public String toString() {
        return "Runway{" + "id=" + id + ", airportId=" + airport + ", surface=" + surface + ", identification=" + identification + '}';
    }
    
}
