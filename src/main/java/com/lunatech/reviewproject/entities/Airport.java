package com.lunatech.reviewproject.entities;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author nvkmi
 */

@Entity
@Table(name = "airports")
public class Airport {

    @Id
    private Long id;
    
    @Column(nullable = false)
    private String name;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy="airport")
    private Set<Runway> runways;

    public Airport() {
    }

    public Airport(Long id, Country country, String name) {
        this.id = id;
        this.country = country;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Runway> getRunways() {
        return runways;
    }

    public void setRunways(Set<Runway> runways) {
        this.runways = runways;
    }

    @Override
    public String toString() {
        return "Airport{" + "id=" + id + ", countryId=" + country + ", name=" + name + '}';
    }
    
}
