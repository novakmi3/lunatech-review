package com.lunatech.reviewproject.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author nvkmi
 */

@Controller
public class HomeController {
    
    private static final String HOME_VIEW = "home";
    
    @RequestMapping("/")
    public String showHome() {   
        return HOME_VIEW;
    }
}
