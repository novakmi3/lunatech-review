package com.lunatech.reviewproject.web;

import com.lunatech.reviewproject.entities.Country;
import com.lunatech.reviewproject.services.QueryService;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author nvkmi
 */

@Controller
public class QueryController {
    
    private static final Pattern NAME_PATTERN = Pattern.compile("[a-zA-Z ]*");
    
    private static final String QUERY_VIEW = "query";

    @Autowired
    private QueryService queryService;
    
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public String showQuery(Country ctr, Model model) {

        Country country = null;

        if (ctr.getName() != null) {
            if (!NAME_PATTERN.matcher(ctr.getName()).matches()) {
                model.addAttribute("error", "Country name cannot contain numbers or special characters.");
                return QUERY_VIEW;
            }
            country = queryService.getAirportsWithRunwaysInCountry(ctr.getName());
        }

        if (country == null) {
            country = new Country();
        }
        
        model.addAttribute("country", country);

        return QUERY_VIEW;
    }
}
