package com.lunatech.reviewproject.web;

import com.lunatech.reviewproject.entities.Country;
import com.lunatech.reviewproject.services.ReportService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author nvkmi
 */

@Controller
public class ReportController {
    
    private static final String REPORT_VIEW = "report";
    
    @Autowired
    private ReportService reportService;
    
    @RequestMapping("/report")
    public String showQuery(Model model)
    {
        List<Country> countriesH = reportService.findCountriesWithHighestNumberOfAirports();
        List<Country> countriesL = reportService.findCountriesWithLowestNumberOfAirports();
        Map<String, List<String>> runways = reportService.findTypeOfRunwayForCountry();
        List<String> commonRunways = reportService.findMostCommonRunways(10);
       
        model.addAttribute("countriesH", countriesH);
        model.addAttribute("countriesL", countriesL);
        model.addAttribute("runways", runways);
        model.addAttribute("commonRunways", commonRunways);
        
        return REPORT_VIEW;
    }
}
