package com.lunatech.reviewproject.services;

import com.lunatech.reviewproject.daos.ReportDao;
import com.lunatech.reviewproject.entities.Country;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nvkmi
 */

@Service
public class ReportService {
    
    @Autowired
    private ReportDao reportDao;
    
    public List<Country> findCountriesWithHighestNumberOfAirports() {
        return reportDao.findCountriesWithHighestNumberOfAirports();
    }
    
    public List<Country> findCountriesWithLowestNumberOfAirports() {
        return reportDao.findCountriesWithLowestNumberOfAirports();
    }
    
    public Map<String, List<String>> findTypeOfRunwayForCountry() {
        
        List<Object[]> results = reportDao.findTypeOfRunwayForCountry();
        return convertResultsToMap(results);
    }
    
    public List<String> findMostCommonRunways(int count) {
        return reportDao.findMostCommonRunways(count);
    }
    
    private Map<String, List<String>> convertResultsToMap(List<Object[]> results) {
        Map<String, List<String>> ret = new TreeMap<>();
        
        for (Object[] row : results) {
            
            String ctry = (String) row[0];
            String runway = (String) row[1];
            
            if (!ret.containsKey(ctry)) {
                ret.put(ctry, new ArrayList<>());
            }   
            ret.get(ctry).add(runway);
        }
        return ret;
    }
}
