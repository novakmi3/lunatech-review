package com.lunatech.reviewproject.services;

import com.lunatech.reviewproject.daos.QueryDao;
import com.lunatech.reviewproject.entities.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nvkmi
 */

@Service
public class QueryService {
    
    @Autowired
    private QueryDao queryDao;
    
    public Country getAirportsWithRunwaysInCountry(String country) {
        return (country == null) ? null : queryDao.findAirportsWithRunwaysInCountry(country);
    }
    
}
