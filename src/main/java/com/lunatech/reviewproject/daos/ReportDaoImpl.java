package com.lunatech.reviewproject.daos;

import com.lunatech.reviewproject.entities.Country;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nvkmi
 */

@Repository
@Transactional
public class ReportDaoImpl implements ReportDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Country> findCountriesWithHighestNumberOfAirports() {
        Session session = sessionFactory.getCurrentSession();

        String hql = "from Country as c"
                + " order by size(c.airports) desc";
        
        return session.createQuery(hql)
                .setMaxResults(10)
                .list();
    }

    @Override
    public List<Country> findCountriesWithLowestNumberOfAirports() {
        Session session = sessionFactory.getCurrentSession();

        String hql = "from Country as c"
                + " order by size(c.airports)";
        
        return session.createQuery(hql)
                .setMaxResults(10)
                .list();
    }

    @Override
    public List<Object[]> findTypeOfRunwayForCountry() {
        Session session = sessionFactory.getCurrentSession();

        String hql = "select distinct c.name, r.surface from Country as c"
                + " join c.airports as a"
                + " join a.runways as r"
                + " order by c.name, r.surface";

        return session.createQuery(hql)
                .list();
    }

    @Override
    public List<String> findMostCommonRunways(int count) {
        Session session = sessionFactory.getCurrentSession();

        String hql = "select identification from Runway as r"
                + " group by r.identification"
                + " order by count(*) desc";
        
        return session.createQuery(hql)
                .setMaxResults(count)
                .list();
    }
}
