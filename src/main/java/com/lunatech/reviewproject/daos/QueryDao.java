package com.lunatech.reviewproject.daos;

import com.lunatech.reviewproject.entities.Country;

/**
 *
 * @author nvkmi
 */

public interface QueryDao {
    
    public Country findAirportsWithRunwaysInCountry(String country);
}
