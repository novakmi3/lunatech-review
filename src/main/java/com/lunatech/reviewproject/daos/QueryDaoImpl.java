package com.lunatech.reviewproject.daos;

import com.lunatech.reviewproject.entities.Country;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nvkmi
 */

@Repository
@Transactional
public class QueryDaoImpl implements QueryDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Country findAirportsWithRunwaysInCountry(String country) {
        Session session = sessionFactory.getCurrentSession();

        String hgl = "from Country as c"
                + " inner join fetch c.airports as a"
                + " inner join fetch a.runways"
                + " where upper(c.name) like :countryName or upper(c.code) like :countryCode"
                + " order by a.name";

        return (Country) session.createQuery(hgl)
                .setParameter("countryCode", country.toUpperCase())
                .setParameter("countryName", country.toUpperCase() + "%")
                .setMaxResults(1)
                .uniqueResult();
    }
}
