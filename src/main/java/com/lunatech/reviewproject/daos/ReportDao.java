package com.lunatech.reviewproject.daos;

import com.lunatech.reviewproject.entities.Country;
import java.util.List;

/**
 *
 * @author nvkmi
 */

public interface ReportDao {
    
    public List<Country> findCountriesWithHighestNumberOfAirports();
    
    public List<Country> findCountriesWithLowestNumberOfAirports();
    
    public List<Object[]> findTypeOfRunwayForCountry();
    
    public List<String> findMostCommonRunways(int count);
}
