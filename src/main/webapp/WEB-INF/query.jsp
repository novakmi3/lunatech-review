<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Query</title>
    </head>
    <body>
        <a href="<spring:url value="/"></spring:url>">Home</a>
        <h1>Query</h1>

        <form:form modelAttribute="country" method="get">
            <form:input path="name" type="text" id="name" />
            
            <button type="submit">Send</button>
        </form:form>
        
        <c:choose>
            <c:when test="${not empty error}">
                    <span style="color: red;"><c:out value="${error}" /></span>
            </c:when>
            <c:otherwise>
                <c:if test="${not empty country}"> 
                    <h2><c:out value="${country.name}" /></h2>
                    <ul>
                        <c:forEach items="${country.airports}" var="airport">
                            <li><c:out value="${airport.name}" />
                                <ul>
                                    <c:forEach items="${airport.runways}" var="runway">
                                        <li><c:out value="${runway.identification}" /></li>
                                    </c:forEach>  
                                </ul>
                            </li>
                        </c:forEach>
                    </ul>
                </c:if>
            </c:otherwise>
        </c:choose>
    </body>
</html>
