<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reports</title>
        <style>
            table {
                border-collapse: collapse;
            }
            
            table td, table th {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <a href="<spring:url value="/"></spring:url>">Home</a>
        <h1>Reports</h1>

        <h2>Countries with highest number of airports</h2>
        <ul>
            <c:forEach items="${countriesH}" var="country">
                <li><c:out value="${country.name}" /></li>
                </c:forEach>
        </ul>

        <h2>Countries with lowest number of airports</h2>
        <ul>
            <c:forEach items="${countriesL}" var="country">
                <li><c:out value="${country.name}" /></li>
                </c:forEach>
        </ul>

        <h2>Type of runways per country</h2>
        <table>
            <tr>
                <th>Country</th>
                <th>Runways</th>
            </tr>
            <c:forEach items="${runways}" var="entry">
                <tr>
                    <td><c:out value="${entry.key}"/></td>
                    <td>
                        <c:forEach items="${entry.value}" var="runway">
                            <c:out value="${runway}"/>&nbsp;
                        </c:forEach>
                    </td>
                </tr> 
            </c:forEach>
        </table>
        
        <h2>10 most common runway identifications</h2>
        <ul>
            <c:forEach items="${commonRunways}" var="identification">
                <li><c:out value="${identification}"/></li>
            </c:forEach>
            
        </ul>
    </body>
</html>
