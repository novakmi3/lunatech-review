<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Lunatech review project</title>
</head>
<body>
<div class="container">
    <h1>Lunatech review project</h1>
    <a href="<spring:url value="/query"></spring:url>">Query</a><br />
    <a href="<spring:url value="/report"></spring:url>">Report</a>
</div>
</body>

</html>